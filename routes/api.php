<?php

use App\Http\Controllers\api\AdminController;
use App\Http\Controllers\api\AuthController;
use App\Http\Controllers\api\LoanCategoryController;
use App\Http\Controllers\api\LoanContractController;
use App\Http\Controllers\api\LoanController;
use App\Http\Controllers\api\UserApplyController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/register', [AuthController::class, 'register'])->name('register');
Route::post('/login', [AuthController::class, 'login'])->name('login');


Route::middleware('auth:api')->group(function (){

    Route::resource('loan-category', LoanCategoryController::class);
    Route::resource('loans', LoanController::class);
    Route::resource('apply', UserApplyController::class);
    Route::resource('loan-contract', LoanContractController::class);
});

Route::middleware(['auth:api', 'admin'])->prefix('admin')->group(function (){

    Route::get('/apply', [AdminController::class, 'applies_list'] );
    Route::get('/apply/{id}', [AdminController::class, 'apply_show'] );
    Route::put('/apply/{id}', [AdminController::class, 'apply_update_status'] );

    Route::get('/contract', [AdminController::class, 'contracts'] );

    Route::get('/contract/{id}', [AdminController::class, 'contract'] );

});
