<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->foreignId('loan_category_id')->constrained('loan_categories')->onUpdate('cascade')->onDelete('cascade');
            $table->double('amount');
            $table->integer('tenure');
            $table->integer('interest_rate');
            $table->integer('processing_fee');
            $table->double('monthly_payment');
            $table->double('total_payment');
            $table->text('requirements');
            $table->boolean('available')->default(True);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
};
