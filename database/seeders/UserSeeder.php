<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([

            'name' => 'admin',
            'email' => 'admin@admin.com',
            'role' => 'admin',
            'password' => Hash::make('admin@123')

        ]);
        DB::table('users')->insert(
            [
                'name' => 'user',
                'email' => 'user@user.com',
                'address' => 'tehran, iran ....',
                'nin' => '0020112596',
                'mobile' => '09121232233',
                'password' => Hash::make('user@123')
            ]
        );
    }
}
