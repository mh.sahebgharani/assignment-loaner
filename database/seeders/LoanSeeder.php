<?php

namespace Database\Seeders;

use App\Models\Loan;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LoanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Loan::insert([
            [
                "title" => "eco marriage loan",
                "loan_category_id" => 2,
                "amount" => 12000,
                "tenure" => 36,
                "interest_rate" => 10,
                "processing_fee" => 500,
                "monthly_payment" => 400,
                "total_payment" => 15000,
                "requirements" => "salary min 24k"
            ],
            [
                "title" => "eco house loan",
                "loan_category_id" => 3,
                "amount" => 300000,
                "tenure" => 60,
                "interest_rate" => 12,
                "processing_fee" => 1500,
                "monthly_payment" => 600,
                "total_payment" => 320000,
                "requirements" => "salary min 44k"
            ],

        ]);
    }
}
