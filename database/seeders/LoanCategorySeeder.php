<?php

namespace Database\Seeders;

use App\Models\LoanCategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class LoanCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        LoanCategory::insert(
            [[
                'title' => 'Repair',
                'description' => 'loan for repairing things',
            ], [
                'title' => 'Marriage',
                'description' => 'loan for couple marriage',
            ], [
                'title' => 'House',
                'description' => 'loan for buying house',
            ]]
        );
    }
}
