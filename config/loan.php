<?php

return [
    // maximum time that user can apply for any loan and wait for result / maximum pending.
    "max_apply" => 1,

    // maximum un payed contract that user can reach, if user reach that number then he/she cant apply anymore.
    "max_contract" => 3,
];
