# Loaner

The loaner project is a web application that provides different types of loan such as marriage loan, house loan , etc. that users can see all available
loans,see the requirements and apply for any of them base on their need.

## Getting started

First step clone the project on your local drive then follow the Installation down blow.

## Installation

### Method 1. With Docker

1. Install [docker](https://docs.docker.com/engine/installation/) and [docker-compose](https://docs.docker.com/compose/install/).

2. There is docker-compose.yml file in the project folder, you can edit it according to your needs.

3. From your project directory, start up your application by running:

```sh
docker-compose up -d
```

after a while you should see:

* Creating assignment-loaner_db_1 ... done
* Creating assignment-loaner_app_1 ... done
* Creating assignment-loaner_redis_1 ... done


4. after the docker container goes live run this command to access the app terminal:

```sh
docker exec -it assignment-loaner_app_1 bash
```

5. in app terminal run this command to install all packages:

```sh
composer install
```

6. then run migration command to migrate tables to:

```sh
php artisan migrate
```

7. then run seed command to seed the tables:

```sh
php artisan db:seed
```

8. now app is live and ready to use.
   
   to test api routes you can use `loans_app.postman_collection.json` file
   in `postman` folder of the project, import it on
   postman application and test the api routes.
   
   or you can use `swagger` documentation on path `/api/documentation`


9. for starting the queue for email and other services use this command:( if you want to get emails )

```
php artisan queue:work
```
you can also change queue connection from redis to database
by editing the `.env` file and `config/queue.php`

you also can access to database cli and redis cli by:
```
//mysql db
docker exec -it assignment-loaner_db_1 bash

//redis
docker exec -it assignment-loaner_redis_1 bash
```
 you also can stop the container by:
```
docker-compose stop
```
if you changed any configure on docker files you should dothe following commands:

```
docker-compose down
docker-compose build
docker-compose up
```
### Method 2. local environment
1. You need php:8.0 & mysql and redis on your local drive, then follow the rest:
   
2. in project directory terminal run this command to install all packages:

```sh
composer install
```

3. then run migration command to migrate tables to:

```sh
php artisan migrate
```

4. then run seed command to seed the tables:

```sh
php artisan db:seed
```
5. start the server with:

```
php artisan serve
```

6. now app is live and ready to use to test api routes you can use loans_app.postman_collection.json file in postman folder of the project, import it on
   postman application and test the api routes.


7. for starting the queue for email and other services use this command:( if you want to get emails )

```
php artisan queue:work
```
## Url & Ports

ports maped on your local machine

```
loan app => 127.0.0.1:8080
database => 127.0.0.1:3308
redis => 127.0.0.1:6379
```

you can configure all of them in .env file of the project.

## Authentication

login as user:

```
email => user@user.com
pass => user@123 
```

login as admin:

```
email => admin@admin.com
pass => admin@123 
```

app auth is JWT authentication after Login you will receive an `access_token` use it as Authorization in request Header like:

```
Bearer access_token
```

you can register new user to receive emails on register route and other events of the app.

```
method: post
url: localhost:8080/api/register
json data keys:
 {
    "name" => "testUser",
    "address" => "test Address",
    "mobile" => "09101011111",
    "nin" => "0020102222",
    "email" => "test@test.com",
    "password" => "test@1234",
    "password_confirmation" => "test@1234"
 }

```

## Test 

For testing if all routes working correctly you can use this command:

```
php artisan test
```
***

## Author
MH.Sahebgharani 

contact email:
    m.sahebgharani90@gmail.com
## Usage

It's place to find loan.

## License

MIT License

