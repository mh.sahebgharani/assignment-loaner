<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class AuthController extends Controller
{
    /**
     * @OA\Post(
     *     path="/api/register",
     *     tags={"Auth"},
     *     summary="Register User",
     *     description="Register User",
     *     operationId="postRegister",
     *     @OA\RequestBody(
                @OA\JsonContent(
                    type="object",
                    @OA\Property(property="name", type="string", example="hassan"),
                    @OA\Property(property="email", type="string",example="hassan@gmail.com"),
                    @OA\Property(property="address", type="string",example="hassan street"),
                    @OA\Property(property="mobile", type="integer",example="09121112233"),
                    @OA\Property(property="nin", type="integer",example="0010102564"),
                    @OA\Property(property="password", type="string",example="hassan@123"),
                    @OA\Property(property="password_confirmation", type="string", example="hassan@123")
                )
            ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *             @OA\JsonContent(
     *                 type="object",
     *                 @OA\Property(property="msq", type="string", example="your registration has been successfully completed"),
     *             )
     *     ),
     * )
     */

    public function register(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'mobile' => 'required|regex:/(09)[0-9]{9}/|unique:users',
            'nin' => 'required|digits:10|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:8',
        ]);

        User::create(array_merge($request->all(),['password'=>Hash::make($request->password)]));

        return response()->json(['msg' => 'your registration has been successfully completed']);
    }

    /**
     * @OA\Post(
     *     path="/api/login",
     *     tags={"Auth"},
     *     summary="Login User",
     *     description="Login User",
     *     operationId="postLogin",
     *     @OA\RequestBody(
                @OA\JsonContent(
                    type="object",
                    @OA\Property(property="email", type="string", example="user@user.com"),
                    @OA\Property(property="password", type="string",example="user@123")
                )
            ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *         @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="msq", type="string", example="login successful"),
     *              @OA\Property(property="access_token", type="string", example="ecdfsd....")
     *         )
     *     ),
     * )
     */
    public function login(Request $request)
    {

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $token = auth()->attempt(['email' => $request->email, "password" => $request->password]);
        if (!$token) {
            return response()->json(['msg' => 'invalid credential'], 401 );
        }

        return response()->json(['msg' => 'login successful', 'access_token'=> $token]);
    }
}
