<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Resources\api\ApplyCollection;
use App\Http\Resources\api\ApplyResource;
use App\Services\api\ApplyService;
use Illuminate\Http\Request;

class UserApplyController extends Controller
{

    protected $apply;
    public function __construct(ApplyService $service)
    {
        $this->apply = $service;
        $this->middleware('auth:api');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    /**
     * @OA\Get(
     *      path="/api/apply",
     *      operationId="getApplies",
     *      tags={"User"},
     *      summary="Get list of Applies",
     *      description="Returns list of Applies",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *     security={{"Bearer":{}}}
     *     )
     */
    public function index()
    {
        //
        $data = new ApplyCollection(\request()->user()->apply);

        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */

    /**
     * @OA\Post(
     *     path="/api/apply",
     *     tags={"User"},
     *     summary="create apply",
     *     description="create apply",
     *     operationId="postApply",
     *     @OA\RequestBody(
     *     @OA\JsonContent(
     *     type="object",
     *     @OA\Property(property="loan_id", type="integer", example="2"),
     *     @OA\Property(property="user_application", type="string",example="user_application"),*     )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *             @OA\JsonContent(
     *                 type="object",
     *                 @OA\Property(property="msq", type="string", example="you applied successfully"),
     *                 @OA\Property(property="data", type="object", example="{...}"),
     *             )
     *     ),
     * )
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
           'loan_id' => 'required|exists:loans,id',
           'user_application' => 'required',
        ]);

        $data = $this->apply->store($request);

        return response()->json($data, $data['status']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */

    /**
     * @OA\Get(
     *      path="/api/apply/{id}",
     *      operationId="getApply",
     *      tags={"User"},
     *      summary="Get a apply",
     *      description="Returns q apply",
     *      @OA\Parameter(name="id",in="path",required=true),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found"
     *      ),
     *     security={{"Bearer":{}}}
     *     )
     */
    public function show($id)
    {
        //
        $data = new ApplyResource(\request()->user()->apply()->find($id));

        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
