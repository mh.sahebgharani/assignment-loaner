<?php

namespace App\Http\Controllers\api;

use App\Events\ContractPaymentEvent;
use App\Http\Controllers\Controller;
use App\Models\LoanContract;
use Illuminate\Http\Request;

class LoanContractController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('admin')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    /**
     * @OA\Get(
     *      path="/api/loan-contract",
     *      operationId="getUserContracts",
     *      tags={"User"},
     *      summary="Get User Contracts",
     *      description="Returns User Contracts",
     *      @OA\Parameter(name="id",in="path",required=true),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found"
     *      ),
     *     security={{"Bearer":{}}}
     *     )
     */
    public function index()
    {
        //
        $data = \request()->user()->contract()->paginate();

        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    /**
     * @OA\Post(
     *     path="/api/loan-contract",
     *     tags={"Admin"},
     *     summary="create contract",
     *     description="create contract",
     *     operationId="postContract",
     *     @OA\RequestBody(
     *     @OA\JsonContent(
     *     type="object",
     *     @OA\Property(property="user_id", type="integer", example="2"),
     *     @OA\Property(property="contract", type="string",example="contract hash"),
     *     )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *             @OA\JsonContent(
     *                 type="object",
     *                 @OA\Property(property="msq", type="string", example="you applied successfully"),
     *                 @OA\Property(property="data", type="object", example="{...}"),
     *             )
     *     ),
     * )
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
            'contract' => 'required'
        ]);

        $contract = LoanContract::create([
            'user_id' => $request->user_id,
            'contract' => $request->contract
        ]);

        return response()->json(['msg' => 'contact added successfully', 'data' => $contract]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\LoanContract $loanContract
     * @return \Illuminate\Http\JsonResponse
     */

    /**
     * @OA\Get(
     *      path="/api/loan-contract/{id}",
     *      operationId="getaContract",
     *      tags={"User"},
     *      summary="Get a contract",
     *      description="Returns a contract",
     *      @OA\Parameter(name="id",in="path",required=true),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found"
     *      ),
     *     security={{"Bearer":{}}}
     *     )
     */
    public function show(LoanContract $loanContract)
    {
        //
        $data = \request()->user()->contract()->find($loanContract->id);
        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\LoanContract $loanContract
     * @return \Illuminate\Http\Response
     */
    public function edit(LoanContract $loanContract)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\LoanContract $loanContract
     * @return \Illuminate\Http\JsonResponse
     */

    /**
     * @OA\Put(
     *     path="/api/loan-contract/{id}",
     *     tags={"Admin"},
     *     summary="update contract",
     *     description="update contract",
     *     operationId="putContract",
     *     @OA\Parameter(name="id",in="path",required=true),
     *     @OA\RequestBody(
     *     @OA\JsonContent(
     *     type="object",
     *     @OA\Property(property="payment", type="integer", example="1"),
     *     )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *             @OA\JsonContent(
     *                 type="object",
     *                 @OA\Property(property="msq", type="string", example="updated successfully"),
     *                 @OA\Property(property="data", type="object", example="{...}"),
     *             )
     *     ),
     * )
     */
    public function update(Request $request, LoanContract $loanContract)
    {
        //
        $loanContract->update([
            'payment' => $request->payment
        ]);

        event(new ContractPaymentEvent($loanContract));

        return response()->json(['msg' => 'contract updated successfully', 'data' => $loanContract]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\LoanContract $loanContract
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoanContract $loanContract)
    {
        //
    }
}
