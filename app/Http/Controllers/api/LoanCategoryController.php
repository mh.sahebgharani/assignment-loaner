<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\LoanCategory;
use Illuminate\Http\Request;

class LoanCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('admin')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    /**
     * @OA\Get(
     *      path="/api/loan-category",
     *      operationId="getLoanCategoryList",
     *      tags={"Admin","User"},
     *      summary="Get list of categories",
     *      description="Returns list of categories",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *     security={{"Bearer":{}}}
     *     )
     */
    public function index()
    {
        $data = LoanCategory::paginate();

        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    /**
     * @OA\Post(
     *     path="/api/loan-category",
     *     tags={"Admin"},
     *     summary="create category",
     *     description="create category",
     *     operationId="postCategory",
     *     @OA\RequestBody(
     *     @OA\JsonContent(
     *     type="object",
     *     @OA\Property(property="title", type="string", example="category 2"),
     *     @OA\Property(property="description", type="string",example="category 2 description"),
     *     )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *             @OA\JsonContent(
     *                 type="object",
     *                 @OA\Property(property="msq", type="string", example="stored successfully"),
     *                 @OA\Property(property="data", type="object", example="{...}"),
     *             )
     *     ),
     * )
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'title' => 'required|unique:loan_categories',
            'description' => 'required',
        ]);
        $loanCategory = LoanCategory::create($request->all());

        return response()->json(['msg' => 'category stored', 'category' => $loanCategory]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LoanCategory  $loanCategory
     * @return \Illuminate\Http\JsonResponse
     */
    /**
     * @OA\Get(
     *      path="/api/loan-category/{id}",
     *      operationId="getLoanCategory",
     *      tags={"Admin","User"},
     *      summary="Get a category",
     *      description="Get a category",
     *      @OA\Parameter(name="id",in="path",required=true),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *     security={{"Bearer":{}}}
     *     )
     */
    public function show(LoanCategory $loanCategory)
    {
        //
        return response()->json($loanCategory);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LoanCategory  $loanCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(LoanCategory $loanCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LoanCategory  $loanCategory
     * @return \Illuminate\Http\JsonResponse
     */
    /**
     * @OA\Put(
     *     path="/api/loan-category/{id}",
     *     tags={"Admin"},
     *     summary="update category",
     *     description="update category",
     *     operationId="putCategory",
     *     @OA\Parameter(name="id",in="path",required=true),
     *     @OA\RequestBody(
     *     @OA\JsonContent(
     *     type="object",
     *     @OA\Property(property="title", type="string", example="category 3"),
     *     @OA\Property(property="description", type="string",example="cate 3")
     *     )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *             @OA\JsonContent(
     *                 type="object",
     *                 @OA\Property(property="msq", type="string", example="updated successfully"),
     *                 @OA\Property(property="data", type="object", example="{...}"),
     *             )
     *     ),
     * )
     */
    public function update(Request $request, LoanCategory $loanCategory)
    {
        //
        $this->validate($request, [
            'title' => 'required|unique:loan_categories',
            'description' => 'required',
        ]);
        $loanCategory->update($request->all());

        return response()->json(['msg' => 'updated successfully', 'data' => $loanCategory]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LoanCategory  $loanCategory
     * @return \Illuminate\Http\JsonResponse
     */

    /**
     * @OA\Delete (
     *     path="/api/loan-category/{id}",
     *     tags={"Admin"},
     *     summary="delete category",
     *     description="delete category",
     *     operationId="deleteCategory",
     *     @OA\Parameter(name="id",in="path",required=true),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *             @OA\JsonContent(
     *                 type="object",
     *                 @OA\Property(property="msq", type="string", example="deleted successfully"),
     *                 @OA\Property(property="data", type="object", example="{...}"),
     *             )
     *     ),
     * )
     */
    public function destroy(LoanCategory $loanCategory)
    {
        //
        $loanCategory->delete();
        return response()->json(['msg' => 'deleted successfully']);
    }
}
