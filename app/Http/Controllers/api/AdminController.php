<?php

namespace App\Http\Controllers\api;

use App\Events\UserApplyEvent;
use App\Http\Resources\api\ApplyCollection;
use App\Http\Resources\api\ApplyResource;
use App\Models\LoanContract;
use App\Models\User;
use App\Models\UserApply;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * @OA\Get(
     *      path="/api/admin/apply",
     *      operationId="getAllApplies",
     *      tags={"Admin"},
     *      summary="Get list of Applies",
     *      description="Returns list of Applies",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *     security={{"Bearer":{}}}
     *     )
     */
    public function applies_list()
    {

        $data = new ApplyCollection(UserApply::paginate(10));

        return response()->json($data);
    }
    /**
     * @OA\Get(
     *      path="/api/admin/apply/{id}",
     *      operationId="getaApply",
     *      tags={"Admin"},
     *      summary="Get a apply",
     *      description="Returns q apply",
     *      @OA\Parameter(name="id",in="path",required=true),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found"
     *      ),
     *     security={{"Bearer":{}}}
     *     )
     */
    public function apply_show($id)
    {

        $data = new ApplyResource(UserApply::find($id));

        return response()->json($data);
    }
    /**
     * @OA\Put(
     *     path="/api/admin/apply/{id}",
     *     tags={"Admin"},
     *     summary="update apply",
     *     description="update apply",
     *     operationId="putApply",
     *     @OA\Parameter(name="id",in="path",required=true),
     *     @OA\RequestBody(
     *     @OA\JsonContent(
     *     type="object",
     *     @OA\Property(property="status", type="string", example="accept"),
     *     )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *             @OA\JsonContent(
     *                 type="object",
     *                 @OA\Property(property="msq", type="string", example="updated successfully"),
     *                 @OA\Property(property="data", type="object", example="{...}"),
     *             )
     *     ),
     * )
     */
    public function apply_update_status(Request $request, $id)
    {

        $this->validate($request, [
            'status' => 'required|in:accept,reject'
        ]);

        UserApply::find($id)->update($request->all());

        $data = UserApply::find($id);

        event(new UserApplyEvent($data));
        return response()->json(['msg' => 'updated successfully', 'data' => $data]);
    }

    /**
     * @OA\Get(
     *      path="/api/admin/contract",
     *      operationId="getAllContracts",
     *      tags={"Admin"},
     *      summary="Get list of Contracts",
     *      description="Returns list of Contracts",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *     security={{"Bearer":{}}}
     *     )
     */
    public function contracts()
    {
        $data = LoanContract::paginate(10);

        return response()->json($data);
    }
    /**
     * @OA\Get(
     *      path="/api/admin/contract/{id}",
     *      operationId="getContracts",
     *      tags={"Admin"},
     *      summary="Get a Contract",
     *      description="Returns q Contract",
     *      @OA\Parameter(name="id",in="path",required=true),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found"
     *      ),
     *     security={{"Bearer":{}}}
     *     )
     */
    public function contract($id)
    {
        $data = LoanContract::find($id);

        return response()->json($data);
    }
}
