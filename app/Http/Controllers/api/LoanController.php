<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Services\api\LoanService;
use Illuminate\Http\Request;

class LoanController extends Controller
{
    protected $loans;

    public function __construct(LoanService $service)
    {
        $this->loans = $service;
        $this->middleware('auth:api');
        $this->middleware('admin')->except(['index', 'show']);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    /**
     * @OA\Get(
     *      path="/api/loans",
     *      operationId="getLoans",
     *      tags={"User","Admin"},
     *      summary="Get list of projects",
     *      description="Returns list of projects",
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *     security={{"Bearer":{}}}
     *     )
     */
    public function index()
    {
        //
        $data = $this->loans->getLoans();
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    /**
     * @OA\Post(
     *     path="/api/loans",
     *     tags={"Admin"},
     *     summary="create loan",
     *     description="create loan",
     *     operationId="postLoan",
     *     @OA\RequestBody(
     *     @OA\JsonContent(
     *     type="object",
     *     @OA\Property(property="title", type="string", example="loan1t2"),
     *     @OA\Property(property="loan_category_id", type="string",example="1"),
     *     @OA\Property(property="amount", type="integer",example="120000"),
     *     @OA\Property(property="tenure", type="integer",example="12"),
     *     @OA\Property(property="interest_rate", type="integer",example="5"),
     *     @OA\Property(property="processing_fee", type="string",example="300"),
     *     @OA\Property(property="requirements", type="string", example="all type of requirements")
     *     )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *             @OA\JsonContent(
     *                 type="object",
     *                 @OA\Property(property="msq", type="string", example="loan stored successfully"),
     *                 @OA\Property(property="data", type="object", example="{...}"),
     *             )
     *     ),
     * )
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
           'title' => 'required|unique:loans',
           'loan_category_id' => 'required|exists:loan_categories,id',
           'amount' => 'required|numeric',
           'tenure' => 'required|numeric',
           'interest_rate' => 'required|numeric',
           'processing_fee' => 'required|numeric',
           'requirements' => 'required',
        ]);
        $data = $this->loans->create($request->all());
        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    /**
     * @OA\Get(
     *      path="/api/loans/{id}",
     *      operationId="getLoan",
     *      tags={"User","Admin"},
     *      summary="Get list of projects",
     *      description="Returns list of projects",
     *      @OA\Parameter(name="id",in="path",required=true),
     *      @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *       ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=404,
     *          description="Not Found"
     *      ),
     *     security={{"Bearer":{}}}
     *     )
     */
    public function show($id)
    {
        //
        $data = $this->loans->getLoan($id);
        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    /**
     * @OA\Put(
     *     path="/api/loans/{id}",
     *     tags={"Admin"},
     *     summary="update loan",
     *     description="update loan",
     *     operationId="putLoan",
     *     @OA\Parameter(name="id",in="path",required=true),
     *     @OA\RequestBody(
     *     @OA\JsonContent(
     *     type="object",
     *     @OA\Property(property="title", type="string", example="loan1t2"),
     *     @OA\Property(property="loan_category_id", type="string",example="1"),
     *     @OA\Property(property="amount", type="integer",example="120000"),
     *     @OA\Property(property="tenure", type="integer",example="12"),
     *     @OA\Property(property="interest_rate", type="integer",example="5"),
     *     @OA\Property(property="processing_fee", type="string",example="300"),
     *     @OA\Property(property="requirements", type="string", example="all type of requirements")
     *     )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *             @OA\JsonContent(
     *                 type="object",
     *                 @OA\Property(property="msq", type="string", example="loan updated successfully"),
     *                 @OA\Property(property="data", type="object", example="{...}"),
     *             )
     *     ),
     * )
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'title' => 'required',
            'loan_category_id' => 'required|exists:loan_categories,id',
            'amount' => 'required|numeric',
            'tenure' => 'required|numeric',
            'interest_rate' => 'required|numeric',
            'processing_fee' => 'required|numeric',
//           'monthly_payment' => 'required',
            'requirements' => 'required',
        ]);

        $data = $this->loans->update($request->all(), $id);

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */

    /**
     * @OA\Delete (
     *     path="/api/loans/{id}",
     *     tags={"Admin"},
     *     summary="delete loan",
     *     description="delete loan",
     *     operationId="deleteLoan",
     *     @OA\Parameter(name="id",in="path",required=true),
     *     @OA\Response(
     *         response=200,
     *         description="successful operation",
     *             @OA\JsonContent(
     *                 type="object",
     *                 @OA\Property(property="msq", type="string", example="loan deleted successfully"),
     *                 @OA\Property(property="data", type="object", example="{...}"),
     *             )
     *     ),
     * )
     */
    public function destroy($id)
    {
        //
        $data = $this->loans->delete($id);

        return response()->json($data);
    }
}
