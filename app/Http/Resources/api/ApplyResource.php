<?php

namespace App\Http\Resources\api;

use Illuminate\Http\Resources\Json\JsonResource;

class ApplyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "user" => $this->when($request->user()->isAdmin(), $this->user),
            "loan" => $this->loan->title,
            "status" => $this->status,
            "user_application" => $this->user_application,
            "admin_note" => $this->when($this->status == "reject", $this->admin_note),
            "created_at" => "2022-04-14T15=>52=>36.000000Z",
            "updated_at" => "2022-04-14T15=>52=>36.000000Z"
        ];
    }
}
