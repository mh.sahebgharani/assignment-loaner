<?php

namespace App\Http\Resources\api;

use Illuminate\Http\Resources\Json\JsonResource;

class LoanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'category' => $this->category->title,
            'amount' => $this->amount,
            'tenure' => $this->tenure,
            'processing_fee' => $this->processing_fee,
            'monthly_payment' => $this->monthly_payment,
            'requirements' => $this->tenure,
//            'apply' => route('apply.store', ['loan_id'=>$this->id])
//            'link' => route('loans.show',$this->id)
        ];
    }
}
