<?php

namespace App\Http\Resources\api;

use Illuminate\Http\Resources\Json\ResourceCollection;

class LoanCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function($data) {
                return [
                    'id' => $data->id,
                    'title' => $data->title,
                    'category' => $data->category->title,
                    'amount' => $data->amount,
                    'tenure' => $data->tenure,
                    'link' => route('loans.show',$data->id)
                ];
            })
        ];
    }
}
