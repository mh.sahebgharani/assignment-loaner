<?php

namespace App\Http\Resources\api;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ApplyCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function($data) use ($request) {
                return [
                    'id' => $data->id,
                    "user" => $this->when($request->user()->isAdmin(), $data->user->email),
                    "loan" => $data->loan->title,
                    "status" => $data->status,
                    'link' => route('apply.show',$data->id)
                ];
            })
        ];
    }
}
