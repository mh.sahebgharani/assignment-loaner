<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendPaymentComplete extends Mailable
{
    use Queueable, SerializesModels;
    public $contract;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($contract)
    {
        //
        $this->contract = $contract;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'name' => $this->contract->user->name,
            'title' => $this->contract->contract
        ];
        return $this->subject('Payment Complete')->view('mail.payment-complete', $data);
    }
}
