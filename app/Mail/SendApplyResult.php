<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendApplyResult extends Mailable
{
    use Queueable, SerializesModels;
    public $apply;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($apply)
    {
        //
        $this->apply = $apply;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $status = $this->apply->status;

        $name = $this->apply->user->name;
        $title = $this->apply->loan->title;
        $reason = $this->apply->admin_note;

        $data = ['name' => $name, 'title' => $title];

        if ($status == "accept"){

            return $this->subject('Apply Confirmation')->view('mail.apply-confirmation', $data);
        }
        else{
            $data[ 'reason'] = $reason;
            return $this->subject('Apply Rejection')->view('mail.apply-rejection', $data);
        }

    }
}
