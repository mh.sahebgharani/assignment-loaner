<?php

namespace App\Listeners;

use App\Events\UserApplyEvent;

use App\Mail\SendApplyResult;
use App\Mail\SendWelcomeMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class UserApplyListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserApplyEvent $event)
    {
        //
        $email = $event->apply->user->email;
        Mail::to($email)->send(new SendApplyResult($event->apply));

    }
}
