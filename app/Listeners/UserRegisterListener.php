<?php

namespace App\Listeners;

use App\Events\UserRegisterEvent;
use App\Mail\SendWelcomeMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class UserRegisterListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function handle(UserRegisterEvent $event)
    {
        //
        $email = $event->user->email;

        Mail::to($email)->send(new SendWelcomeMail($event->user));
    }
}
