<?php

namespace App\Listeners;

use App\Mail\SendPaymentComplete;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class ContractPaymentListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        //
        $email = $event->contract->user->email;
        if ($event->contract->payment == 1){
            Mail::to($email)->send(new SendPaymentComplete($event->contract));
        }
    }
}
