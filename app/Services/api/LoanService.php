<?php


namespace App\Services\api;

use App\Http\Resources\api\LoanCollection;
use App\Http\Resources\api\LoanResource;
use App\Models\Loan;
use Spatie\FlareClient\Http\Exceptions\NotFound;

class LoanService
{

    public function getLoans()
    {
        return new LoanCollection(Loan::where('available', True)->paginate(10));
    }

    public function getLoan($id)
    {
        $loan = Loan::findOrFail($id);

        return new LoanResource($loan);

    }

    public function create($inputs)
    {
        $payments = $this->loanCalculator($inputs);

        $loan = Loan::create(array_merge($inputs, ['monthly_payment' => $payments['monthly_payment'], 'total_payment' => $payments['total_payment']]));

        return ['msg' => 'loan stored successfully', 'data' => $loan];
    }

    public function update($inputs, $id)
    {
        $payments = $this->loanCalculator($inputs);

        $loan = Loan::findOrFail($id);

        $loan->update(array_merge($inputs, ['monthly_payment' => $payments['monthly_payment'], 'total_payment' => $payments['total_payment']]));

        return ['msg' => 'loan updated successfully', 'data' => $loan];
    }

    public function delete($id)
    {
        $loan = Loan::findOrFail($id);

        $loan->delete();

        return ['msg' => 'loan deleted successfully'];
    }

    // method for auto calculation of monthly payment for user
    protected function loanCalculator($inputs)
    {
        $amount = $inputs['amount'];
        $percentage = $inputs['interest_rate'];
        $duration = $inputs['tenure'];
        $processing_fee = $inputs['processing_fee'];

        $total_payment = ((($amount * $percentage * ($duration + 1)) / 2400) + $amount) + $processing_fee;

        $monthly_payment = ($amount * $percentage / 1200 * (pow((1 + $percentage / 1200), $duration))) / ((pow((1 + $percentage / 1200), $duration)) - 1);
        $monthly_payment = round($monthly_payment, 2);
        return ['total_payment' => $total_payment, 'monthly_payment' => $monthly_payment];
    }
}
