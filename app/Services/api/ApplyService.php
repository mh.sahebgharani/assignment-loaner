<?php


namespace App\Services\api;


class ApplyService
{
    // if user has already applied for [n] loan cant apply for more.
    protected $max_apply;

    // if user has already has  [n] loan contract -> cant apply for more.
    protected $max_contract;

    public function __construct()
    {
        $this->max_apply = config('loan.max_apply');
        $this->max_contract = config('loan.max_contract');
    }

    public function store($request)
    {
        $contract = $this->contract_checker($request->user());
        $pending = $this->pending_checker($request->user());


        if (!$contract["eligible"] || !$pending["eligible"]) {

            $reason = $contract["reason"] . ' ' . $pending["reason"];

            return ["msg" => "you can't apply for any more loans", "reason" => $reason, "status"=>403];
        }

        $data = $request->user()->apply()->create($request->all());

        return ["msg" => "you applied successfully", "data" => $data,"status"=>200];
    }

    protected function contract_checker($user)
    {

        if ($user->contract()->count() >= $this->max_contract) {
            //user has maximum under payment contract -> can't apply
            return ["eligible" => 0, "reason" => "reached max contract"];
        }

        //can apply
        return ["eligible" => 1, "reason" => null];

    }

    protected function pending_checker($user)
    {

        if ($user->apply()->count() >= $this->max_apply) {
            //user has maximum pending apply -> can't apply
            return ["eligible" => 0, "reason" => "reached max pending"];
        }

        //can apply
        return ["eligible" => 1, "reason" => null];
    }
}
