<?php

namespace App\Providers;

use App\Events\ContractPaymentEvent;
use App\Events\UserApplyEvent;
use App\Events\UserRegisterEvent;
use App\Listeners\ContractPaymentListener;
use App\Listeners\UserApplyListener;
use App\Listeners\UserRegisterListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
//        Registered::class => [
//            SendEmailVerificationNotification::class,
//        ],
        UserRegisterEvent::class => [
            UserRegisterListener::class
        ],
        UserApplyEvent::class => [
            UserApplyListener::class
        ],
        ContractPaymentEvent::class => [
            ContractPaymentListener::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
