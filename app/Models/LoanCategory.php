<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanCategory extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
    ];

    public function loans()
    {
        return $this->hasMany(Loan::class, 'loan_category_id');
    }
}
