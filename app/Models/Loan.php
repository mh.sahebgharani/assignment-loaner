<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'loan_category_id',
        'amount',
        'tenure',
        'interest_rate',
        'processing_fee',
        'monthly_payment',
        'total_payment',
        'requirements',
        'available',
    ];

    public function category(){
        return $this->belongsTo(LoanCategory::class, 'loan_category_id');
    }

    public function applied(){
        return $this->hasMany(UserApply::class, 'loan_id');
    }
}
