<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_register()
    {
        $response = $this->postJson('/api/register', [
            "name" => "testUser",
            "address" => "test Address",
            "mobile" => "09101011111",
            "nin" => "0020102222",
            "email" => "test@test.com",
            "password" => "test@1234",
            "password_confirmation" => "test@1234"
        ]);

        $response->assertStatus(200);
    }

    public function test_login(){
        $response = $this->postJson('/api/login', [
            'email' => 'test@test.com',
            'password' => 'test@1234'
        ]);
        $response->assertStatus(200);

        $this->assertArrayHasKey('access_token',$response->json());

        User::where('email','test@test.com')->delete();
    }
}
