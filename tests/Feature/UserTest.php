<?php

namespace Tests\Feature;

use App\Models\Loan;
use App\Models\User;
use App\Models\UserApply;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;

class UserTest extends TestCase
{

    /**
     * A basic feature test example.
     *
     * @return void
     */
    protected function login()
    {
        $response = $this->postJson('/api/login', [
            'email' => 'user@user.com',
            'password' => 'user@123'
        ]);

        return $response->json('access_token');
    }

    public function test_get_loans()
    {
        $access_token = $this->login();
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $access_token,
        ])->getJson('/api/loans');

        $response->assertStatus(200);
    }

    public function test_single_loan()
    {
        $access_token = $this->login();
        $loan_id = Loan::first()->id;

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $access_token,
        ])->getJson('/api/loans/' . $loan_id);

        $response->assertStatus(200);
    }

    public function test_apply_loan()
    {
        $access_token = $this->login();
        $loan_id = Loan::first()->id;

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $access_token,
        ])->postJson('/api/apply', [
            "loan_id" => $loan_id,
            "user_application" => "test apply"
        ]);
        $response->assertStatus(200);
    }

    public function test_get_applies()
    {
        $access_token = $this->login();
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $access_token,
        ])->getJson('/api/apply');

        $response->assertStatus(200);
    }

    public function test_get_single_apply()
    {
        $access_token = $this->login();

        $id = User::where('email','user@user.com')->first()->apply()->first()->id;

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $access_token,
        ])->getJson('/api/apply/'.$id);

        $response->assertStatus(200);

        UserApply::find($id)->delete();
    }
}
