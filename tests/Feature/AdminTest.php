<?php

namespace Tests\Feature;

use App\Models\Loan;
use App\Models\LoanCategory;
use App\Models\LoanContract;
use App\Models\User;
use App\Models\UserApply;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AdminTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    protected function login()
    {
        $response = $this->postJson('/api/login', [
            'email' => 'admin@admin.com',
            'password' => 'admin@123'
        ]);

        return $response->json('access_token');
    }

    public function test_create_loan_category()
    {
        $access_token = $this->login();

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $access_token,
        ])->postJson('/api/loan-category', [
            "title" => "test-cat",
            "description" => "test desc",
        ]);

        $response->assertStatus(200);

        LoanCategory::where('title', 'test-cat')->delete();
    }

    public function test_create_loan()
    {
        $access_token = $this->login();

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $access_token,
        ])->postJson('/api/loans', [
            'title' => 'test-loan-1',
            'loan_category_id' => 1,
            'amount' => 12000,
            'tenure' => 60,
            'interest_rate' => 10,
            'processing_fee' => 500,
            'requirements' => 'background check',
        ]);

        $response->assertStatus(200);

        Loan::where('title', 'test-loan-1')->delete();
    }

    public function test_update_apply()
    {
        $access_token = $this->login();

        $apply = UserApply::first();
        $id = '';
        if (!$apply) {
            $loan_id = Loan::first()->id;
            $response = $this->withHeaders([
                'Authorization' => 'Bearer ' . $access_token,
            ])->postJson('/api/apply', [
                "loan_id" => $loan_id,
                "user_application" => "test apply"
            ]);
            $id = $response->json('data')['id'];
        } else {
            $id = $apply->id;
        }

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $access_token,
        ])->putJson('/api/admin/apply/' . $id, [
            "status" => "accept",
        ]);

        $response->assertStatus(200);

        UserApply::find($id)->delete();;
    }

    public function test_create_update_contract()
    {
        $access_token = $this->login();

        $user_id = User::first()->id;
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $access_token,
        ])->postJson('/api/loan-contract', [
            "user_id" => $user_id,
            "contract" => "test contract"
        ]);


        $response->assertStatus(200);

        $id = $response->json('data')['id'];

        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $access_token,
        ])->putJson('/api/loan-contract/' . $id, [
            "payment" => 1,
        ]);

        $response->assertStatus(200);

        LoanContract::find($id)->delete();
    }
}
